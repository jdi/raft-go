# Raft implementation in Go

The objective of this project is to start bootstrapping a Raft implementation using Go. More specifically, and since I haven't yet decided whether I want to use Go or not for the full implementation of the algorithm, in this repository I'll be focusing only on the leader election part of Raft.

Together with this repository, there will be other ones where I'll be experimenting with other programming languages until I find the one that better fits what I expect from it [the language] so that I can continue with the full implementation.

## What is here to see?

For running the project just run the following command on the root folder:

```
$ go run main.go
```

In `main.go` you'll find the main procedure (as the name suggest) while on `raft/server.go` you'll encounter the "meat" of the project.

## Tasks

- [X] Start adding tests.
- [ ] Find out whether the `currentTerm` field of the candidate server is being accessed concurrently.
- [ ] Divide the Server from its internals and its interface that communicates with the outside world.

## Other implementations

- [Ada](https://gitlab.com/jdi/raft-ada)
- [Pony](https://gitlab.com/jdi/raft-pony)
