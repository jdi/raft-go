package raft

import (
	"math/rand"
	"sync"
	"time"

	"github.com/Sirupsen/logrus"
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

//go:generate stringer -type=role
type role int

const (
	follower role = iota
	candidate
	leader
)

const (
	minElectionDuration  = 250
	minHeartbeatDuration = minElectionDuration / 10
)

// Server is a Raft server, handling the consensus protocoll in a group of
// peers.
type Server struct {
	id     int
	logger *logrus.Logger

	m              sync.Mutex
	role           role
	currentTerm    int
	currentLeader  int
	votedFor       *int
	peers          map[int]Peer
	commitIndex    int
	lastApplied    int
	electionTicker *time.Ticker
}

// New returns a new Server pointer with the provided id and logger.
func New(id int, logger *logrus.Logger) *Server {
	return &Server{
		id:             id,
		logger:         logger,
		peers:          make(map[int]Peer),
		electionTicker: createTicker(minElectionDuration), // TODO we should not need this to be done here
	}
}

// TODO refactor out, should not be necessary if peers are handled
// more indirectly.
func (s *Server) getId() int {
	return s.id
}

// TODO refactor out, should not be necessary if peers are handled
// more indirectly.
func (s *Server) getRole() role {
	s.m.Lock()
	defer s.m.Unlock()
	return s.role
}

// AddPeer adds the provided peer server to the peer
// map of the server in which the method was called.
func (s *Server) AddPeer(p Peer) {
	// TODO get rid of this method, peers can't be added dynamically and a public
	// method suggesting it could be done shoule be omitted.
	s.peers[p.getId()] = p
}

func (s *Server) requestVote(term, candidateID, lastLogIndex, lastLogTerm int) requestVoteResponse {
	s.m.Lock()
	defer s.m.Unlock()

	if term > s.currentTerm && s.votedFor == nil {
		s.votedFor = &candidateID
		return requestVoteResponse{s.currentTerm, true}
	}

	return requestVoteResponse{s.currentTerm, false}
}

func (s *Server) appendEntries(term int, leaderID int) appendEntriesResponse {
	s.m.Lock()
	defer s.m.Unlock()

	if term < s.currentTerm {
		return appendEntriesResponse{s.currentTerm, false}
	}

	s.logger.WithFields(logrus.Fields{
		"receiver": s.id,
		"role":     s.role.String(),
		"sender":   leaderID,
	}).Debugf("heartbeat")

	s.electionTicker.Stop()
	s.electionTicker = createTicker(minElectionDuration)
	s.currentLeader = leaderID

	return appendEntriesResponse{s.currentTerm, true}
}

func (s *Server) startElection() {
	s.m.Lock()
	defer s.m.Unlock()

	if s.role != follower {
		return
	}

	s.role = candidate
	s.currentTerm++

	votex := &sync.Mutex{}
	votes := 1

	var wg sync.WaitGroup
	for _, peer := range s.peers {
		wg.Add(1)
		go func(p Peer) {
			defer wg.Done()
			res := p.requestVote(s.currentTerm, s.id, 0, 0)
			if res.term > s.currentTerm {
				s.currentTerm = res.term
				s.role = follower
			}
			if !res.voteGranted {
				return
			}
			votex.Lock()
			votes++
			votex.Unlock()
		}(peer)
	}
	wg.Wait()

	if votes > len(s.peers)/2 { // TODO: check what 3 of 7 would result in
		s.role = leader
	}

	s.logger.WithFields(logrus.Fields{
		"server": s.id,
		"term":   s.currentTerm,
		"votes":  votes,
		"role":   s.role.String(),
	}).Debugf("election ended")
}

func (s *Server) sendEntries() {
	if s.role != leader {
		return
	}
	// FIXME s.peers and concurrency, be careful with allocations
	for _, peer := range s.peers {
		go func(p Peer) {
			s.m.Lock()
			defer s.m.Unlock()

			if res := p.appendEntries(s.currentTerm, s.id); !res.success {
				s.currentTerm = res.term
				s.role = follower
			}
		}(peer)
	}
}

// Loop uses the electionTicker and heartbeatTicker
// to coordinate the action that the Server should perform.
// FIXME direct access to electionTicker and concurrency may not work that well together
func (s *Server) Loop() {
	heartbeatTicker := createTicker(minHeartbeatDuration)
	for {
		select {
		case <-s.electionTicker.C:
			s.startElection()
		case <-heartbeatTicker.C:
			s.sendEntries()
		}
	}
}

func createTicker(d time.Duration) *time.Ticker {
	return time.NewTicker(time.Duration(rand.Intn(int(d)))*time.Millisecond + d)
}
