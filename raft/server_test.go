package raft

import (
	"testing"
	"time"

	"github.com/Sirupsen/logrus"
)

func TestServer(t *testing.T) {
	// Loggers
	logger1 := logrus.New()
	logger2 := logrus.New()
	logger3 := logrus.New()
	// Spawn servers
	server1 := New(1, logger1)
	server2 := New(2, logger2)
	server3 := New(3, logger3)

	// Add peers to server1
	server1.AddPeer(server2)
	server1.AddPeer(server3)
	t.Run("Server starts as follower", func(t *testing.T) {
		if r := server1.getRole(); r != follower {
			t.Fatalf("Expected: %q, got: %q", follower, r)
		}
	})
	t.Run("Peers are added correctly", func(t *testing.T) {
		if server1.peers[server2.getId()] == nil {
			t.Fatal("Server", server2.getId(), "was not added correctly.")
		}
		if server1.peers[server3.getId()] == nil {
			t.Fatal("Server", server3.getId(), "was not added correctly.")
		}
	})
	t.Run("Server 1 becomes the leader", func(t *testing.T) {
		go server1.Loop()
		time.Sleep(time.Second * 1)
		if r := server1.getRole(); r != leader {
			t.Fatalf("Expected: %q, got: %q", leader, r)
		}
	})
}

type testServer interface {
	getId() int
	getKind() string
}
