package raft

// TODO Peer needs documentation
// TODO If a peer only implements private methods, it should be private too
type Peer interface {
	getId() int
	requestVote(term, candidateID, lastLogIndex, lastLogTerm int) requestVoteResponse
	appendEntries(term int, leaderID int) appendEntriesResponse
}

type requestVoteResponse struct {
	term        int
	voteGranted bool
}

type appendEntriesResponse struct {
	term    int
	success bool
}
